import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.Reader;
import org.json.simple.*;
import org.json.simple.parser.*;
import org.json.simple.JSONObject;

public class CustomJSONParser {
	
	public static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while((cp = rd.read()) != -1) {
			sb.append((char) cp);
		} // end of while
		
		return sb.toString();
	}	// end of method

	//converts url info to String using Scanner
	public static JSONObject readStringFromURL(String urlString) throws IOException {
        JSONObject apiResponse = new JSONObject();
	    try {
	    	URL url = new URL(urlString);
	    	InputStream is = url.openStream();
	    	Scanner scanner = new Scanner(is);
	        scanner.useDelimiter("\\A"); // start of string delimiter
	        String jsonText = scanner.hasNext() ? scanner.next() : ""; // now next takes all of it

            Object obj = new JSONParser().parse(jsonText);
            apiResponse = (JSONObject) obj;

	    } catch(Exception e) {
	    	e.printStackTrace();
	    }
		return apiResponse;
	}	// end of method
	
	
	public static void main(String[] args) throws Exception {
        String newsApiUrl = "https://newsapi.org/v2/everything?apiKey=30e0eb72e2204dc684bb64d347765a80";
        String paramsString = "&q=bitcoin&from=2019-08-23&sortBy=publishedAt";
		
		JSONObject urlText = readStringFromURL("https://newsapi.org/v2/everything?q=bitcoin&from=2019-08-27&sortBy=publishedAt&apiKey=30e0eb72e2204dc684bb64d347765a80");
		System.out.println(urlText.get("status"));
        JSONArray articles = (JSONArray) urlText.get("articles");
		// int numArticles = urlText.getInt("totalResults");
		for(int index = 0; index < 25; index++) {
			JSONObject article = (JSONObject) articles.get(index);
			System.out.println("================");
			System.out.println(article.get("title"));
			System.out.println(article.get("author"));
			System.out.println("================");
		}
	}	// end of main
}
