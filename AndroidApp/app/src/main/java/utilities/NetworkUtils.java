package utilities;

import android.net.Uri;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.Scanner;
import java.io.InputStream;
import java.io.IOException;
import android.util.Log;


public class NetworkUtils {
    static final String newsApiUrl = "https://newsapi.org/v2/everything?";

    public static URL buildUrl(String userQuery){
        // Builds the correct request url with the passed in queryString
        Uri builtUri = Uri.parse(newsApiUrl).buildUpon()
                .appendQueryParameter("q", userQuery)
                .appendQueryParameter("from", "2019-08-27")
                .appendQueryParameter("sortBy", "publishedAt")
                .appendQueryParameter("apiKey", "30e0eb72e2204dc684bb64d347765a80")
                .build();
        URL url = null;
        try{
            url = new URL(builtUri.toString());

        }catch (MalformedURLException e){
            e.printStackTrace();
        }
        return url;
    }   // end of method buildUrl

    // Makes the API request and returns the entire response as a string
    public static String getResponseFromHttpUrl(URL url) throws IOException{
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        try {
            InputStream in = urlConnection.getInputStream();
            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");
            boolean hasInput = scanner.hasNext();
            if (hasInput) return scanner.next();
            else return null;
        } finally {
            urlConnection.disconnect();
        }
    }   // end of method
}
