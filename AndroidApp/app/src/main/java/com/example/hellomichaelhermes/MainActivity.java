package com.example.hellomichaelhermes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.health.SystemHealthManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.os.Bundle;
import android.widget.*;
import android.util.Log;

import android.os.AsyncTask;
import org.json.JSONArray; // different from org.json.simple
import org.json.JSONException;
import org.json.JSONObject;

import utilities.NetworkUtils;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private TextView mSearchResultsTextView;
    private TextView mDisplayTextView;
    private EditText mSearchBoxEditText;
    private Button mSearchButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSearchResultsTextView = (TextView) findViewById(R.id.tv_results);
        mDisplayTextView = (TextView) findViewById(R.id.tv_display_text);
        mSearchBoxEditText = (EditText) findViewById(R.id.et_search_box);
        mSearchButton = (Button) findViewById(R.id.button_search);

        //detect and respond to button click
        mSearchButton.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View v){ //onClick() is an inner method definition. CLOSURE
                        // this code is executed when button is clicked
                        String searchText = mSearchBoxEditText.getText().toString();
                        Log.d("informational","Search Button Clicked!");
                        String msg = "Searching for " + searchText;
                        mSearchResultsTextView.setText(msg);
                        Context c = MainActivity.this;
                        Toast.makeText(c,msg, Toast.LENGTH_LONG).show(); //makeText -- Static Method

                        makeSearchQuery();
                    }// end of inner method
                }//end of object View.OnClickListener

        ); // end of setOnClickListener
    } //end of OnCreate

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main, menu);

        return true;

    } // end

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int menuItemSelected = item.getItemId();
        if(menuItemSelected == R.id.action_about){ //TODO
            Context context = MainActivity.this;
            String message = "Search selected";
            Log.d("informational",message);
            Toast.makeText(context,message,Toast.LENGTH_LONG).show();
        }
        return true;
    }

    // TODO Function makeSearchQuery
    private void makeSearchQuery(){
        String searchQuery = mSearchBoxEditText.getText().toString();
        mDisplayTextView.setText("Results for " + searchQuery);
        mSearchResultsTextView.setText(""); // empty string
        System.out.println("made it into make search Query");
        new FetchNetworkData().execute(searchQuery);

    } // end of method makeSearchQuery


    public class FetchNetworkData extends AsyncTask<String, Void, String> {

        @Override
        // Builds URL using our NetworkUtils package and initiates the request
        protected String doInBackground(String... params){
            System.out.println("made it into do in background");
            if (params.length == 0) return null;
            String searchQuery = params[0]; // this will receive whatever is passed to execute

            URL url = NetworkUtils.buildUrl(searchQuery);

            String responseString = null;
            try{
                responseString = NetworkUtils.getResponseFromHttpUrl(url);

            } catch(Exception e){
                e.printStackTrace();
            }
            return responseString;
        }   // end of method doInBackground

        @Override
        // Receives the list of article titles and appends them to the TextView
        protected void onPostExecute(String responseString){
            System.out.println("made it into on post execute");
            String [] titles = processNewsAPIJson(responseString);
            for (String title : titles){
                System.out.println(title);
                mSearchResultsTextView.append(title + "\n\n");
            }
        }   // end onPostExecute method

        // Parses the News API response and returns an Array of article titles
        public String[] processNewsAPIJson(String responseJsonData){
            try {
                // Get all articles, determine how many there are, and create an Array to store them
                JSONObject newsResponseObject = new JSONObject(responseJsonData);
                JSONArray allArticles = newsResponseObject.getJSONArray("articles");
                int numOfArticles = allArticles.length();
                String[] newsTitles = new String [numOfArticles];

                // Retrieve titles from each of the Articles and store them in the Array created above
                for (int i = 0; i < numOfArticles; i++){
                    JSONObject article = allArticles.getJSONObject(i);
                    String title = article.getString("title");
                    newsTitles[i] = title;
                    System.out.println(title);
                }   // end of for loop
            }catch(JSONException e){
                e.printStackTrace();
            }
            return new String[0];
        }   // end of method processRedditJson


    }   // end of class FetchNetworkData



} // end of Class
